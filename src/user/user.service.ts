import { Injectable, BadRequestException} from '@nestjs/common';
import { AddUserDto } from './dto/add-user.dto';
import { UserRepository } from './user.repository';
import { Filter } from './dto/filter.dto';
import { UpdateDto } from './dto/update.dto';

@Injectable()
export class UserService {
    constructor(private readonly userRepository: UserRepository){}

    addUser(addUserDto: AddUserDto) {
        

        this.userRepository.find({username:addUserDto.username}).then(value=>{
            if (value.length!=0){
                throw new BadRequestException('username already exists')
            }
        })
        
        this.userRepository.find({email:addUserDto.email}).then(value=>{
            if (value.length!=0){
                throw new BadRequestException('email already exists')
            }
        })

        let y = new Date()
        let x = new Date(addUserDto.birthday)

        if(Math.floor((y.getTime()-x.getTime())/31556952000)<18){
            throw new BadRequestException('age is < 18')
        }
        
        return this.userRepository.save(addUserDto)
    }

    findUsers(filter: Filter){
        const {q, gender, page, limit} = filter
        let x = {take:limit,skip: (page-1)*limit}

        if (q && gender){
            return this.userRepository.find({where: [{first_name:q, gender:gender},
                {last_name:q, gender: gender}, {username:q, gender:gender}], ...x})
        } else if (gender){
            return this.userRepository.find({where:{gender:gender},...x })
        } else if (q){
            return this.userRepository.find({where: [{first_name:q},
                {last_name:q}, {username:q}], ...x})
        }

        return this.userRepository.find(x)
    }

    getUserById(id:string){
        return this.userRepository.find({id:id})
    }

    UpdateUserById(id:string, updateDto: UpdateDto){
        return this.userRepository.update(id,updateDto)
    }
}
