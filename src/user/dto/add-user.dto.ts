import { IsNotEmpty, IsAlpha, MaxLength, IsEmail, IsOptional, IsDate, IsEnum, IsAlphanumeric, Validate, Validator} from 'class-validator'
import { Type } from 'class-transformer'
import { Gender } from './gender.dto';
import { Unique } from 'typeorm';
import { UserEntity } from '../entity/user.entity';
import { UserRepository } from '../user.repository';


export class AddUserDto {
    
    @IsAlpha()
    @MaxLength(60)
    @IsNotEmpty()
    first_name: string;

    @IsNotEmpty()
    @IsAlpha()
    @MaxLength(60)
    last_name: string;

    @IsNotEmpty()
    @MaxLength(32)
    @IsAlphanumeric()
    username: string;

    @IsEmail()
    @IsOptional()
    email?: string;

    @IsDate()
    @Type(() => Date)
    @IsOptional()
    birthday?: Date;

    @IsEnum(Gender)
    gender?: string;

}

