import { IsAlpha, MaxLength, IsOptional, IsAlphanumeric, 
 IsEmail, IsDate, IsEnum} from "class-validator";
import { Type } from "class-transformer";
import { Gender } from "./gender.dto";

export class UpdateDto {
    @IsAlpha()
    @MaxLength(60)
    @IsOptional()
    first_name: string;

    @IsAlpha()
    @MaxLength(60)
    @IsOptional()
    last_name: string;

    @MaxLength(32)
    @IsAlphanumeric()
    @IsOptional()
    username: string;

    @IsEmail()
    @IsOptional()
    email?: string;

    @IsDate()
    @Type(() => Date)
    @IsOptional()
    birthday?: Date;

    @IsEnum(Gender)
    gender?: string;
}