import { IsEnum, IsOptional } from "class-validator";
import { Gender } from "./gender.dto";

export class Filter {
    @IsOptional()
    q?: string;

    @IsEnum(Gender)
    @IsOptional()
    gender?: string;

    page: number;

    limit: number;
}