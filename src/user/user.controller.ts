import { Controller, Post, Body, Get, Query, Param, Patch } from '@nestjs/common';
import { AddUserDto } from './dto/add-user.dto';
import { Filter } from './dto/filter.dto';
import { UserService } from './user.service';
import { UpdateDto } from './dto/update.dto';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService){

    }

    @Get()
    addUser(@Body() addUserDto: AddUserDto){
        return this.userService.addUser(addUserDto)
    }

    @Post()
    getUser(@Query() filter: Filter){
        return this.userService.findUsers(filter);
    }

    @Post('/:id')
    getUserById(@Param('id') id:string){
        return this.userService.getUserById(id)
    }

    @Patch('/:id')
    UpdateUserById(@Param('id') id:string, @Body()updateDto: UpdateDto){
        return this.userService.UpdateUserById(id, updateDto)
    }
}
